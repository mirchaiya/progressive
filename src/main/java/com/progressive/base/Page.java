package com.progressive.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.progressive.utilities.DbManager;
import com.progressive.utilities.ExcelReader;
import com.progressive.utilities.ScreenshotUtil;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Page {

	public static WebDriver driver;
	public static Logger log = Logger.getLogger(Page.class.getName());
	public static Properties OR = new Properties();
	public static Properties Config = new Properties();
	public static FileInputStream fis;
	public static ExcelReader excel = new ExcelReader(".\\src\\test\\resources\\excel\\testdata.xlsx");
	public static WebDriverWait wait;
	static WebElement dropdown;
	public static ThreadLocal<ExtentTest> testReport = new ThreadLocal<ExtentTest>();

	public void selectfromlist(String locator, String data) {

		WebElement ul = driver.findElement(By.xpath(OR.getProperty(locator)));
		List<WebElement> allOptions = ul.findElements(By.tagName("li"));
		for (WebElement selectLi : allOptions) {
			selectLi.getText();
			if (selectLi.getText().equals(data)) {
				selectLi.click();
				log.info("Clicking on an : " + data);
				//testReport.get().log(Status.INFO, "Selecting data from Dropdown " + data);
			}
			
		}
	

	}

	public static void getText(String locator) {

		if (locator.endsWith("_XPATH")) {

			log.info("Text from  Element: " + driver.findElement(By.xpath(OR.getProperty(locator))).getText());

		} else if (locator.endsWith("_ID")) {

			log.info("Text from  Element: " + driver.findElement(By.id(OR.getProperty(locator))).getText());

		} else if (locator.endsWith("_CSS")) {

			log.info("Text from  Element: " + driver.findElement(By.cssSelector(OR.getProperty(locator))).getText());

		}

	}

	public static void click(String locator) {

		if (locator.endsWith("_XPATH")) {

			driver.findElement(By.xpath(OR.getProperty(locator))).click();
		} else if (locator.endsWith("_ID")) {

			driver.findElement(By.id(OR.getProperty(locator))).click();
		} else if (locator.endsWith("_CSS")) {

			driver.findElement(By.cssSelector(OR.getProperty(locator))).click();
		}

		log.info("Clicking on an Element: " + locator);
		testReport.get().log(Status.INFO, "Clicking on an Element: " + locator);
	}

	public static void verifyEquals(String expected, String actual) throws IOException {

		try {

			Assert.assertEquals(actual, expected);

		} catch (Throwable t) {

			ScreenshotUtil.captureScreenshot();

			// Extent Reports
			testReport.get().log(Status.FAIL, " Verification failed with exception :" + t.getMessage());
			// ExtentListeners.test.log(Status.FAIL,Test.addScreenCapture(ScreenshotUtil.captureScreenshot()));

		}

	}

	public static void select(String locator, String value) {

		if (locator.endsWith("_XPATH")) {

			dropdown = driver.findElement(By.xpath(OR.getProperty(locator)));
		} else if (locator.endsWith("_ID")) {

			dropdown = driver.findElement(By.id(OR.getProperty(locator)));
		} else if (locator.endsWith("_CSS")) {

			dropdown = driver.findElement(By.cssSelector(OR.getProperty(locator)));
		}

		Select select = new Select(dropdown);
		select.selectByVisibleText(value);

		log.info("Selecting the value from dropdown " + locator + "  selected value as : " + value);
		testReport.get().log(Status.INFO,
				"Selecting the value from dropdown " + locator + " selected value as : " + value);
	}

	public static boolean isElementPresent(String locator) {

		try {
			if (locator.endsWith("_XPATH")) {

				driver.findElement(By.xpath(OR.getProperty(locator)));
			} else if (locator.endsWith("_ID")) {

				driver.findElement(By.id(OR.getProperty(locator)));
			} else if (locator.endsWith("_CSS")) {

				driver.findElement(By.cssSelector(OR.getProperty(locator)));
			}
			log.info("Finding an Element: " + locator);
			testReport.get().log(Status.INFO, "Finding an Element: " + locator);

			return true;
		} catch (Throwable t) {
			log.error("Error while finding an Element: " + locator + "--Error log is :" + t.getMessage());
			testReport.get().log(Status.ERROR,
					"Error while finding an Element: " + locator + "--Error log is :" + t.getMessage());

			return false;
		}

	}

	public static void type(String locator, String value) {

		if (locator.endsWith("_XPATH")) {

			driver.findElement(By.xpath(OR.getProperty(locator))).sendKeys(value);
		} else if (locator.endsWith("_ID")) {

			driver.findElement(By.id(OR.getProperty(locator))).sendKeys(value);
			;
		} else if (locator.endsWith("_CSS")) {

			driver.findElement(By.cssSelector(OR.getProperty(locator))).sendKeys(value);
		}

		log.info("Typing in an Element: " + locator + " entered the value as : " + value);
		testReport.get().log(Status.INFO, "Typing in an Element: " + locator + "entered the value as : " + value);

	}

	
	public Page() {

		if (driver == null) {

			PropertyConfigurator.configure(".\\src\\test\\resources\\properties\\log4j.properties");

			try {
				fis = new FileInputStream(".\\src\\test\\resources\\properties\\OR.properties");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				OR.load(fis);
				log.info("OR Properties loaded !!!");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				fis = new FileInputStream(".\\src\\test\\resources\\properties\\Config.properties");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				Config.load(fis);
				log.info("Config Properties loaded !!!");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (Config.getProperty("browser").equals("chrome")) {

				WebDriverManager.chromedriver().setup();

				log.info("Chrome Browser Launched !!!");
				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("profile.default_content_setting_values.notifications", 2);
				prefs.put("credentials_enable_service", false);
				prefs.put("profile.password_manager_enabled", false);
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("prefs", prefs);
				options.addArguments("--disable-extensions");
				options.addArguments("--disable-infobars");
				driver = new ChromeDriver(options);

			} else if (Config.getProperty("browser").equals("firefox")) {

				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
				log.info("Firefox Browser Launched !!!");
			}

			driver.get(Config.getProperty("testsiteurl"));
			log.info("Navigating to the URL : " + Config.getProperty("testsiteurl"));
			DbManager.setMysqlDbConnection();
			log.info("Database connection established");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Integer.parseInt(Config.getProperty("implicit.wait")),
					TimeUnit.SECONDS);

			wait = new WebDriverWait(driver, Integer.parseInt(Config.getProperty("explicit.wait")));

		}

	}

	@AfterSuite
	public static void quit() {
		if (!(driver == null)) {
			driver.quit();
			log.info("Test Execution completed !!!");
			testReport.get().log(Status.INFO, "Test Execution completed !!!");
		}
	}
}
https://bitbucket.org/Kanakpatti/progressiveinsurance/src/master/ProgressiveAutomation/
