package com.progressive.pages;

import com.progressive.base.Page;

public class AddVehiclePage extends Page {

	public void tellUsAboutBanner() {
		getText("tellusaboutbanner_XPATH");
	}

	public void vehicleYear(String data) {

		selectfromlist("vehicleyear_XPATH", data);
	}

	public void vehicleMake(String data) {

		selectfromlist("vehiclemake_XPATH", data);
	}

	public void vehicleModel(String data) {

		selectfromlist("vehiclemodel_XPATH", data);
	}

	public void primaryUse(String value) {

		select("primaryusedropdown_XPATH", value);
	}

	public void ownOrLeaseOrFinance(String value) {

		select("ownorleaseorfinancedropdwn_XPATH", value);
	}

	public void howLongYouhadVehicle(String value) {

		select("howlongvehiclewithyoudropdwn_XPATH", value);
	}

	public void advancedDriverBanner() {
		getText("advanceddriverbannerlabel_XPATH");
	}
	
	public void gotoDoneBtn() {
		click("donebutton_XPATH");
		
	}
	public void gotoContinueBtn() {
		click("continuebutton_XPATH");
	}

}
