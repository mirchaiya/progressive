package com.progressive.pages;

import com.progressive.base.Page;

public class HomePage extends Page {

	public void gotoAutoplusHome() {

		click("autoplushomelink_XPATH");

	}

	public void gotoAuto() {

		click("autolink_XPATH");

	}

}
