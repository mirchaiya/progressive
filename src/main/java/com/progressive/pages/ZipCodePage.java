package com.progressive.pages;

import java.io.IOException;
import com.progressive.base.Page;

public class ZipCodePage extends Page{

	public void enterZipCode(String zipcode) throws IOException {
		
		type("zipcodetextbox_XPATH",zipcode);
		
	}
	
	public void clickOnGetaQuoteBtn() {
		click("getaquoteBtn_XPATH");
	}
	
	

	
	
}
