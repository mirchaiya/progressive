package com.progressive.pages;
import com.progressive.base.Page;

public class NameAndAddressPage extends Page {

	public void verifybanner() {

		getText("pagetitle_XPATH");
	}

	public void fisrtName(String fristname) {

		type("firstname_XPATH", fristname);

	}

	public void middleName(String middlename) {

		type("middlename_XPATH", middlename);

	}

	public void lastName(String lastname) {

		type("lastname_XPATH", lastname);
	}

	public void dob(String DOB) {

		type("dateofbirth_XPATH", DOB);

	}

	public void verifybannerMailAddress() {
		getText("mailaddresslabel_XPATH");
	}

	public void address(String address) {

		type("address_XPATH", address);

	}

	public void apartment(String apt) {

		click("apt_XPATH");
		type("apt_XPATH", apt);
		

	}

	public void city(String city) {

		type("city_XPATH", city);

	}

	public void zipCode(String zipCode) {

		type("zipcode_XPATH", zipCode);

	}

	public void poBoxCheckBox() {

		click("poboxcheckbox_XPATH");

	}

	public void okayStartMyQuote() {
		click("startmyquotebtn_XPATH");
	}

}
