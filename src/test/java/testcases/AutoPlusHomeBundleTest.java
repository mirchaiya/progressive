package testcases;


import java.io.IOException;
import org.testng.annotations.Test;

import com.progressive.base.Page;
import com.progressive.pages.AddVehiclePage;
import com.progressive.pages.HomePage;
import com.progressive.pages.NameAndAddressPage;
import com.progressive.pages.ZipCodePage;



public class AutoPlusHomeBundleTest extends Page{
	
	@Test(priority =1)
	public void autoplusHomeBundleTest() throws IOException {
		HomePage hp = new HomePage();
		hp.gotoAutoplusHome();
		ZipCodePage ezc = new ZipCodePage();
		ezc.enterZipCode("75060");
		ezc.clickOnGetaQuoteBtn();
	}
	
	@Test(priority =2)
	public void customerInformation() {
		
		NameAndAddressPage info= new NameAndAddressPage();
		info.verifybanner();
		info.fisrtName("Virat");
		info.middleName("M");
		info.lastName("Kholi");
		info.dob("12/08/1991");
		
		info.apartment("2010");
		info.address("1456 Preakness Dr");
		info.apartment("2010");
		info.address("1456 Preakness Dr");
		info.poBoxCheckBox();
		info.okayStartMyQuote();
			
	}
	

	@Test(priority =3)
	public void addVehicleTest() throws InterruptedException {
		AddVehiclePage vehicleinfo=new AddVehiclePage();
		vehicleinfo.tellUsAboutBanner();
		vehicleinfo.vehicleYear("2016");
		vehicleinfo.vehicleMake("Audi");
		vehicleinfo.vehicleModel("A4");
		vehicleinfo.primaryUse("Business (sales calls, business errands, driving clients/products)");
		vehicleinfo.ownOrLeaseOrFinance("Finance");
		Thread.sleep(1500);
		vehicleinfo.howLongYouhadVehicle("1 month - 1 year");
		vehicleinfo.tellUsAboutBanner();
		vehicleinfo.gotoDoneBtn();
		Thread.sleep(1500);
		vehicleinfo.gotoContinueBtn();
		
	}
	
}
